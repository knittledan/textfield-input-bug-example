import 'package:flutter_web/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: 'Discover Brindle',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: Scaffold(
        body: Column(
          children:[
            TextField(
              maxLines: 1,
              decoration: InputDecoration.collapsed(
                  hintText: 'TextField'
              ),
            ),
            SizedBox(height: 10),
            TextFormField(
              maxLines: 1,
              decoration: InputDecoration.collapsed(
                  hintText: 'TextFormField'
              ),
            ),
          ],
        ),
      ),
    );
  }
}
